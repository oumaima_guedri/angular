import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BodycombatRoutingModule } from './bodycombat-routing.module';
import { BodycombatComponent } from './bodycombat.component';


@NgModule({
  declarations: [
    BodycombatComponent
  ],
  imports: [
    CommonModule,
    BodycombatRoutingModule
  ]
})
export class BodycombatModule { }
