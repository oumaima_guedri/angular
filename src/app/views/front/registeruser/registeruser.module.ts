import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisteruserRoutingModule } from './registeruser-routing.module';
import { RegisteruserComponent } from './registeruser.component';


@NgModule({
  declarations: [
    RegisteruserComponent
  ],
  imports: [
    CommonModule,
    RegisteruserRoutingModule
  ]
})
export class RegisteruserModule { }
