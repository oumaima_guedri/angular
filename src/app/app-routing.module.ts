import { AllutilisateursComponent } from './views/admin/allutilisateurs/allutilisateurs.component';
import { TaekwondoModule } from './views/front/taekwondo/taekwondo.module';
import { AdminLayoutComponent } from './layout/admin-layout/admin-layout.component';
import { NgModule } from '@angular/core';
import { RouterModule,Routes } from '@angular/router';
import { FrontLayoutComponent } from './layout/front-layout/front-layout.component';
import { LoginAdminLayoutComponent } from './layout/login-admin-layout/login-admin-layout.component';

const routes :Routes =[
  {path:'' , component:FrontLayoutComponent , children:[
    {path:'', loadChildren:()=>import('./views/front/home/home.module').then(m=>m.HomeModule)},
    {path:'registeruser', loadChildren:()=>import('./views/front/registeruser/registeruser.module').then(m=>m.RegisteruserModule)},
    {path:'loginuser', loadChildren:()=>import('./views/front/loginuser/loginuser.module').then(m=>m.LoginuserModule)},
    {path:'activite', loadChildren:()=>import('./views/front/activite/activite.module').then(m=>m.ActiviteModule)},
    {path:'bodyattack', loadChildren:()=>import('./views/front/bodyattack/bodyattack.module').then(m=>m.BodyattackModule)},
    {path:'bodycombat', loadChildren:()=>import('./views/front/bodycombat/bodycombat.module').then(m=>m.BodycombatModule)},
    {path:'rpm', loadChildren:()=>import('./views/front/rpm/rpm.module').then(m=>m.RpmModule)},
    {path:'fitness', loadChildren:()=>import('./views/front/fitness/fitness.module').then(m=>m.FitnessModule)},
    {path:'cours', loadChildren:()=>import('./views/front/cours/cours.module').then(m=>m.CoursModule)},
    {path:'taekwondo', loadChildren:()=>import('./views/front/taekwondo/taekwondo.module').then(m=>m.TaekwondoModule)},
    {path:'zumba', loadChildren:()=>import('./views/front/zumba/zumba.module').then(m=>m.ZumbaModule)},










  ]},
  {path:'admin' , component:AdminLayoutComponent , children:[
    {path:'',loadChildren:()=>import ('./views/admin/dashboard/dashboard.module').then(m=>m.DashboardModule)},
    {path:'dashboard',loadChildren:()=>import ('./views/admin/dashboard/dashboard.module').then(m=>m.DashboardModule)},
    {path:'allutilisateurs',loadChildren:()=>import ('./views/admin/allutilisateurs/allutilisateurs.module').then(m=>m.AllutilisateursModule)},

    // {path:'login',loadChildren:()=>import ('./views/admin/loginadmin/loginadmin.module').then(m=>m.LoginadminModule)}

  ]},


   {path:'admin/loginadmin',component:LoginAdminLayoutComponent}


];

@NgModule({

  exports:[RouterModule],
  imports: [
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule { }
